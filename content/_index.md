Some Learning activities I did in past

- Implemented a minimal Linux system by compiling the latest kernel source downloaded from
    kernel.org and building a tiny root file system.

- [Reading K&R and systematically working out the exercises in each chapter](https://github.com/syamgk/ANSI_C_KandR)

- [Learning basic linked list / tree manipulation in C](https://github.com/syamgk/Data\_Structures) by working out the exercises in the [Stanford CS library](http://cslibrary.stanford.edu)

- [Learning the Linux system call interface by implementing a handful of basic Linux commands (including xargs, tee, recursive listing etc)](https://github.com/syamgk/unix_cmds.toys)

- Wrote a small [httpserver](https://github.com/syamgk/socket-programming/tree/master/httpserver), [echoserver](https://github.com/syamgk/socket-programming/tree/master/echo_server) written in C

- [Implemented a chat program in C; using the “select” system call.](https://github.com/syamgk/socket-programming}{https://github.com/syamgk/socket-programming)

- [A Game called “Matching cards” written on python using pygame module](https://github.com/syamgk/Matching-cards}{https://github.com/syamgk/Matching-cards)
