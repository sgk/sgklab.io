---
title: About this site
subtitle: coding for fun & bun
comments: false
---
Hi I'm Syam G Krishnan an Engineering graduate.
I am involved in Free software and related activities for the past 3 years, 
and is currently working with technologies related to DevOps at Red Hat.
I have also volunteered at various events related to FLOSS at multiple capacities.
Other than computers my life induces exploring altered states of human minds, organic farming and herbs.
