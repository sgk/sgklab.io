---
title : devconf 2018
date: 2018-02-06
---

This was my first devconf and am so glad that it was special 
because it is the 10th episode and there were fireworks.

The day 1, 
Started with a keynote on by Chris Wright,
he started from fundamentals of open-source 
which is collaboration, trust, initiative and diversity
and the impact open-source and knowledge sharing made in the software industry
then companies started to adopt it and opensource become mainstream
And when opensource become mainstream, the VC's started creating
buzz around the new opensource projects, which gets hype 
and from his perceptive these hypes are 
not going to help evloution as they are not sustainable.
Then he continued on to clouds build with opensource software, 
but with proprietary patches which never got pushed to upstream.
It was scary when he said "is this the end of open-source?"

He also mentioned briefly about the new technologies 
on which redhat is invested in.

After the keynote I tried to get into the rooms where
container related technologies are happening 
and those rooms were full before I reached the other building

Then i got into a workshop "Managing Openshift and Beyond"
where they explained a way to deploy Openshift
using ansible tower and Provisioning Cloud Forms 
to manage and monitor the cloud infra.
https://github.com/sabre1041/managing-ocp-install-beyond/blob/rhte/labs/lab0/lab0.adoc#lab0

then on the second half spent some time at the University's Computer museum.
where I saw Punch cards, PDP 11, Commodore 64. This was some unexpected fun. 

Then later I attended the talk "who needs containers in a serverless world"
by Mattias Luebken and Vašek Pavlín

spoke to few interesting people
met Josh Berkus who is working on scaling postgres on k8s
met few guys who are working on software collections
met a customer who is doing poc of banking related services on openshift.
and at the end of day 1 there was a firework and sparkling wine 

Day 2,
I attended an interesting talk by Dan walsh's on Latest Container Technologies.
where he started with open container initiative 
opened up the D' daemon and introduced skopeo, buildah, libpod
for managing containers 

https://www.youtube.com/watch?v=I0cOn1psf5o



Some randon snaps i made during devconf
https://imgur.com/a/qS7Pk
